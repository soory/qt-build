# Ubuntu
- create .gitlab-ci.yml
- install gitlab-runner on build server
- add gitlab-runner with proper tag and executor to gitlab project
- run below command:
`sudo usermod -aG docker gitlab-runner`
- alow gitlab-runner to run docker commands:\
`usermod -aG docker gitlab-runner`\
`service docker restart`
# Windows server
- install openssh: [tutorial](https://4sysops.com/archives/installing-openssh-on-windows-10-1803-and-higher-and-server-2019/)
- install gitlab-runner on windows server: [tutorial](https://docs.gitlab.com/runner/install/windows.html)
- install docker on windows server: [tutorial](https://computingforgeeks.com/how-to-run-docker-containers-on-windows-server-2019/)
- install [git](https://git-scm.com/download/win) on windows and restarat gitlab-runner `gitlab-runner.exe restart`
- setup docker proxy:
    - edit `C:\ProgramData\Docker\config\daemon.json` file. (create if doesn't already exist)
    - add `[Environment]::SetEnvironmentVariable("HTTP_PROXY", "http://username:password@proxy:port/", [EnvironmentVariableTarget]::Machine)` to daemon.json file.
    - run `Restart-Service docker`

# Gitlab-runner
- gitlab-runner yml reference: [link](https://docs.gitlab.com/ee/ci/yaml/)